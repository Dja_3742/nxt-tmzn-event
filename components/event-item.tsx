import { Component } from 'react'
import styles from '../styles/common.module.css'
import IEvent from '../model/i-event'

export default class EventItem extends Component<IEventProps, any>{
    constructor(props: IEventProps) {
        super(props)
    }

    render() {
        return <li>
            <button className={this.props.isActive ? styles.active : ''} 
                    onClick={this.props.onClick}>
                {this.props.title}
            </button>
        </li>
    }
}

export interface IEventProps { 
    event: IEvent
    onClick: any
    isActive: boolean
    title: string
}

