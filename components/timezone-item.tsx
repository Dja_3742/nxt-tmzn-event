import { Component } from 'react'
import styles from '../styles/common.module.css'
import ITimezone from '../model/i-timezone'

export default class TimezoneItem extends Component<ITimezoneProps, any> {
    constructor(props: ITimezoneProps) {
        super(props)
    }

    render() {
        return <li>
            <button className={this.props.isActive ? styles.active : ''} 
                    onClick={this.props.onClick}>
                {this.props.timezone.name}
            </button>
        </li>
    }
}

export interface ITimezoneProps { 
    timezone: ITimezone
    onClick: any 
    isActive: boolean
}
