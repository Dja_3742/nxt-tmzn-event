import React, { MutableRefObject, useRef, useEffect } from 'react'
import ITimezone from "../model/i-timezone";
import IEvent from "../model/i-event";

const EventCanvas: React.FC<ICanvasProps> = (props: ICanvasProps) => {

    const draw = (canvas: HTMLCanvasElement, tz: ITimezone, e: IEvent) => {
        const ctx = canvas.getContext('2d')
        
        if (ctx) {
            const titleLen = 15
            let chars = Array.from(e.summary)
            let eventTitle = `${chars.slice(0, titleLen).join('')}${chars.length > titleLen ? '...' : ''}`
            
            let eventTime = getEventTime(e.start, e.end, tz.hoursOffset, tz.minutesOffset)
            let multiplier = (e.end.getTime() - e.start.getTime()) / (1000 * 60 * 30)
            multiplier = multiplier < 1 ? 1 : multiplier

            const paddingY = 0
            const paddingX = 10
            
            const cvWidth = 200
            const cardWidth = 120
            const cardHeight = 39 * multiplier;
            
            const h = cardHeight + (2 * paddingY)
            const w = cvWidth + (2 * paddingX)
            const dpr = window.devicePixelRatio * 2

            // prepare canvas
            canvas.width = w * dpr
            canvas.style.width = w + 'px'
            canvas.height = h * dpr
            canvas.style.height = h + 'px'
            ctx.scale(dpr, dpr)
            clearCanvas(ctx)

            // background
            ctx.fillStyle = 'rgba(107,227,127,0.25)'
            ctx.fillRect(paddingX, paddingY, cardWidth, cardHeight)

            // vertical line
            ctx.fillStyle = '#6BE37F'
            ctx.fillRect(paddingX, paddingY, cardWidth / 40, cardHeight)

            // event title 
            ctx.fillStyle = '#39AE4C'
            ctx.font = '800 12px Inter, "Courier New"'
            ctx.fillText(eventTitle, paddingX + 10, 18, w)
            
            // event date 
            ctx.font = '500 11px Inter, "Courier New"'
            ctx.fillText(eventTime, paddingX + 10, 35, w)
        }
    }
    
    function getEventTime(from: Date, to: Date, timezoneHoursOffset: number, timezoneMinutesOffset: number) : string {
        const fromOffset = 
            (
                (timezoneHoursOffset * 60 + timezoneMinutesOffset) // offset from target timezone to UTC              [in minutes]  
                + from.getTimezoneOffset()                         // offset from local timezone to UTC (usually < 0) [in minutes]
            ) * 1000 * 60;                                         // 1000 * 60 represent minutes as milliseconds        [in milliseconds]     
        const toOffset =
            (
                (timezoneHoursOffset * 60 + timezoneMinutesOffset) // offset from target timezone to UTC              [in minutes]  
                + to.getTimezoneOffset()                         // offset from local timezone to UTC (usually < 0) [in minutes]
            ) * 1000 * 60;                                         // 1000 * 60 represent minutes as milliseconds        [in milliseconds]
        
        const fConverted = new Date(from.getTime() + fromOffset)
        const toConverted = new Date(to.getTime() + toOffset)
        
        let hStrFrom = fConverted.getHours().toLocaleString(undefined, {minimumIntegerDigits: 2})
        let mStrFrom = fConverted.getMinutes().toLocaleString(undefined, {minimumIntegerDigits: 2})
        let hStrTo =  toConverted.getHours().toLocaleString(undefined, {minimumIntegerDigits: 2})
        let mStrTo = toConverted.getMinutes().toLocaleString(undefined, {minimumIntegerDigits: 2})

        return `${hStrFrom}:${mStrFrom} – ${hStrTo}:${mStrTo}`
    }
    function clearCanvas(context: CanvasRenderingContext2D | null | undefined) {
        if(context) {
            context.clearRect(0, 0, context.canvas.width, context.canvas.height)
        }
    }
    
    const canvasRef: MutableRefObject<HTMLCanvasElement | null> = useRef(null)
    useEffect(() => {
        const canvas = canvasRef?.current
        if(canvas && props.timezone && props.event) {
            draw(canvas, props.timezone, props.event)
        } else {
            clearCanvas(canvas?.getContext('2d'))
        }
    }, [props])
    
    return <canvas ref={canvasRef} />
}
export default EventCanvas

export interface ICanvasProps { 
    event: IEvent | null
    timezone: ITimezone | null
}
