import ITimezone from './i-timezone'
import IEvent from './i-event'

export default class AppState { 
    selectedTimezone: ITimezone | null
    selectedEvent: IEvent | null
    
    constructor(
        timezone: ITimezone | null,
        event: IEvent | null) 
    {
        this.selectedEvent = event
        this.selectedTimezone = timezone
        this.isTimezoneSelected = this.isTimezoneSelected.bind(this)
        this.isEventSelected = this.isEventSelected.bind(this)
    }
    
    public isTimezoneSelected(timezoneId: string): boolean {
        return this.selectedTimezone?.id === timezoneId
    }
    
    public isEventSelected(eventId: string): boolean { 
        return this.selectedEvent?.id === eventId
    }
}