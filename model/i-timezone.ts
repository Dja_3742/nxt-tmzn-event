export default interface ITimezone {
    id: string
    name: string
    hoursOffset: number
    minutesOffset: number
}