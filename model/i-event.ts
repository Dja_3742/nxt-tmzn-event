export default interface IEvent {
    id: string
    start: Date
    end: Date
    summary: string
}