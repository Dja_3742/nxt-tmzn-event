import Head from 'next/head'
import styles from '../styles/common.module.css'
import evSource from '../data/events.json'
import tzSource from '../data/timezones.json'
import TimezoneItem from '../components/timezone-item'
import EventItem from "../components/event-item"
import AppState from '../model/app-state'
import ITimezone from "../model/i-timezone"
import IEvent from "../model/i-event"
import { Component } from "react"
import EventCanvas from "../components/event-canvas"

export async function getServerSideProps (context: any) {
    let timezones: { id: string, name: string, hoursOffset: number, minutesOffset: number}[] = []
    let events: { id: string, start: string, end: string, summary: string }[] = []

    for (let i = 0; i < tzSource.timezones.length; i++) {
        let tzName = tzSource.timezones[i]
        let offset: {hours: number, minutes: number} = {hours: 0, minutes: 0}
        
        switch (tzName) {
            case 'Europe/Berlin': 
                offset = { hours: 1, minutes: 0}
                break
            case 'Asia/Irkutsk':
                offset = { hours: 8, minutes: 0}
                break
            case 'Pacific/Chatham':
                offset = { hours: 12, minutes: 45} 
                break
        }
        
        let timezoneToPush = {
            id: Date.now().toString(36) + Math.random().toString(36).substr(2),
            name: tzName,
            hoursOffset: offset.hours,
            minutesOffset: offset.minutes
        }
        timezones.push(timezoneToPush)
    }

    for (let j = 0; j < evSource.events.length; j++) {
        let e = evSource.events[j]
        let eventToPush = {
            id: Date.now().toString(36) + Math.random().toString(36).substr(2),
            summary: e.summary,
            start: e.start,
            end: e.end
        }

        events.push(eventToPush)
    }

    return {
        props: {
            timezones: timezones,
            events: events
        }
    }
}

export default class Home extends Component<any, AppState> {
    state = new AppState(null, null)

    handleTimezoneClick (timezone: ITimezone): void  {
        if(this.state.selectedTimezone?.id === timezone.id) {
            this.setState(new AppState(null, this.state.selectedEvent))   
            return
        }
        
        this.setState(new AppState(timezone, this.state.selectedEvent))
    }
    
    handleEventClick (event: IEvent): void {
        if(this.state.selectedEvent?.id === event.id) { 
            this.setState(new AppState(this.state.selectedTimezone, null))
            return
        }
        this.setState(new AppState(this.state.selectedTimezone, event)) 
    }

    render() {
        let tItems = this.props.timezones.map((t: any) => {
            let tz: ITimezone = { id: t.id, name: t.name, hoursOffset: t.hoursOffset, minutesOffset: t.minutesOffset }
            return <TimezoneItem
                key={t.id}
                timezone={tz}
                onClick={() => 
                    this.handleTimezoneClick(tz)}
                isActive={this.state.isTimezoneSelected(t.id)}
            />})
        let eItems = this.props.events.map((e: any) => {
            let event = { id: e.id, start: new Date(e.start), end: new Date(e.end), summary: e.summary }
            return <EventItem 
                key={event.id}
                event={event}
                onClick={() => this.handleEventClick(event)}
                isActive={this.state.isEventSelected(event.id)}
                title={event.summary.substr(0, 14)}
            />
        })
        
        return (
            <div className={styles.container}>
                <Head>
                    <title>Nxt Timezone Event</title>
                    <meta name="description" content=""/>
                    <link rel="icon" href="/favicon.ico"/>
                </Head>
                <main>
                    <h2>Timezoned events</h2>

                    <div className={styles.flexRow}>
                        <div className={styles.flexCol}>
                            <ul className={styles.timezones}>{tItems}</ul>
                        </div>
                        <div className={styles.flexCol}>
                            <ul className={styles.events}>{eItems}</ul>
                        </div>
                    </div>
                    <div className={styles.flexRow}>
                        <div className={styles.flexCol}>
                            <EventCanvas event={this.state.selectedEvent} timezone={this.state.selectedTimezone}/>
                        </div>
                    </div>
                </main>
            </div>
        )
    }
}